const calculator = {
    displayValue: "0",
    firstOperand: null,
    waitingForSecondOperand: false,
    operator: null,
}

function updateDisplay() {

    const display = document.getElementById("display");
    display.innerText = calculator.displayValue;
}
updateDisplay();

const digits = document.querySelectorAll(".number");
const decimal = document.getElementById("decimal");
const operator = document.querySelectorAll(".first-operator");
const otherOperator = document.querySelectorAll(".second-operator")

const myDigitsArray = Array.from(digits);
const myOperatorArray = Array.from(operator);
const mySecondOperator = Array.from(otherOperator);

for (let i = 0; i < myDigitsArray.length; i++) {
    myDigitsArray[i].addEventListener("click", function putAndUpdate() {
        inputDigit(myDigitsArray[i].innerText);
        updateDisplay();
    });
}

for (let i = 0; i < myOperatorArray.length; i++) {
    myOperatorArray[i].addEventListener("click", function operate() {
        handleOperator(myOperatorArray[i].innerText);
    });
}

for (let i = 0; i < mySecondOperator.length; i++) {
    mySecondOperator[i].addEventListener("click", function putOperator() {
        secondOperator(mySecondOperator[i].innerText);
    });
}

decimal.addEventListener("click", function putAndUpdate() {

    inputDigit(".");
    updateDisplay();
})

function inputDigit(digit) {
    const displayValueRead = calculator.displayValue;

    if (calculator.waitingForSecondOperand) {
        calculator.displayValue = digit;
        calculator.waitingForSecondOperand = false;
    } else if (displayValueRead === "0" && digit === ".") {
        calculator.displayValue = "0.";

        return;
    } else if (displayValueRead.includes(".") && digit === ".") {

        return;
    } else if (displayValueRead === "0") {

        calculator.displayValue = digit;

    } else {
        calculator.displayValue = displayValueRead + digit;

    }


}

function secondOperator(secondOperator) {


    if (secondOperator === "AC") {

        calculator.displayValue = "0";
        calculator.firstOperand = null;
        calculator.waitingForSecondOperand = false;
        calculator.operator = null;
        updateDisplay();
    } else if (secondOperator === "del") {

        const readFromObj = calculator.displayValue;


        const newDelete = readFromObj.slice(0, -1);

        if (newDelete === "" || newDelete === "-") {
            calculator.displayValue = "0";
            updateDisplay();
            return
        }

        calculator.displayValue = newDelete;

        updateDisplay();
    } else if (secondOperator === "+ / -") {
        let readyToChangeSign = parseFloat(calculator.displayValue);
        if (readyToChangeSign > 0) {
            readyToChangeSign = -Math.abs(readyToChangeSign);
            calculator.displayValue = readyToChangeSign.toString();
            updateDisplay();
        } else {
            readyToChangeSign = Math.abs(readyToChangeSign);
            calculator.displayValue = readyToChangeSign.toString();
            updateDisplay();
        }


    }

}

function handleOperator(nextOperator) {

    let inputValue = parseFloat(calculator.displayValue);

    if (calculator.operator && calculator.waitingForSecondOperand) {
        calculator.operator = nextOperator;
        console.log(calculator);
        return;
    }

    if (calculator.firstOperand == null && !isNaN(inputValue)) {

        calculator.firstOperand = inputValue;

    } else if (calculator.operator) {

        const result = operate(calculator.firstOperand, inputValue, calculator.operator);
        calculator.firstOperand = result;
        calculator.displayValue = result.toString();
        console.log(result);
        updateDisplay();

    }

    calculator.operator = nextOperator;
    calculator.waitingForSecondOperand = true;
}

function operate(firstNumber, secondNumber, operator) {

    if (operator === "+") {
        return firstNumber + secondNumber;

    } else if (operator === "-") {
        let result = firstNumber - secondNumber;

        return result;
    } else if (operator === "x") {
        let result = firstNumber * secondNumber;

        return result;
    } else if (operator === "/") {
        let result = firstNumber / secondNumber;

        return result;
    }
    return secondNumber;

}